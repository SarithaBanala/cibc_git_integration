package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Order_Form__2_
*/
public class Order_Form__2_ extends PageObjectBase
{

	public Order_Form__2_()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_Password_TEXTBOX_Status,final String Step_1_Password_TEXTBOX_Verification,final String Step_1_Login_BUTTON_Status,final String Step_2_Username_TEXTBOX,final String Step_2_Password_TEXTBOX,final String Step_4_Open_New_Account_HYPERLINK_Status,final String Step_4_Transfer_Funds_HYPERLINK_Status,final String Step_4_Logout_HYPERLINK_Status,final String Step_5_OrderName_TEXTBOX_Status,final String Step_5_OrderName_TEXTBOX_Verification,final String Step_5_Order_HYPERLINK_Status,final String Step_5_Ok_BUTTON_Status,final String Step_6_OrderName_TEXTBOX,final String Step_8_Order_Number_LABEL_Status) throws Exception

	{

	closebrowser_Page closebrowser_page_init=PageFactory.initElements(driver, closebrowser_Page.class);

	S1_Page s1_page_init=PageFactory.initElements(driver, S1_Page.class);

	S2_Page s2_page_init=PageFactory.initElements(driver, S2_Page.class);

	Screen1_Page screen1_page_init=PageFactory.initElements(driver, Screen1_Page.class);

	Screen2_Page screen2_page_init=PageFactory.initElements(driver, Screen2_Page.class);

	CloseBrowser_Page closebrowser_page_init=PageFactory.initElements(driver, CloseBrowser_Page.class);

	RightClick_Page rightclick_page_init=PageFactory.initElements(driver, RightClick_Page.class);

	DisplayMessage_Page displaymessage_page_init=PageFactory.initElements(driver, DisplayMessage_Page.class);

	RightClickOptions_Page rightclickoptions_page_init=PageFactory.initElements(driver, RightClickOptions_Page.class);

	unnamed_Page unnamed_page_init=PageFactory.initElements(driver, unnamed_Page.class);

	Login_Page_Page login_page_page_init=PageFactory.initElements(driver, Login_Page_Page.class);

	Login_Screen_Page login_screen_page_init=PageFactory.initElements(driver, Login_Screen_Page.class);

	Login_Page login_page_init=PageFactory.initElements(driver, Login_Page.class);

	Test1_Page test1_page_init=PageFactory.initElements(driver, Test1_Page.class);

	Account_Services_Page account_services_page_init=PageFactory.initElements(driver, Account_Services_Page.class);

	Screen_Widget_Page screen_widget_page_init=PageFactory.initElements(driver, Screen_Widget_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	OrderDetails_Page orderdetails_page_init=PageFactory.initElements(driver, OrderDetails_Page.class);

	Odrplaced_Page odrplaced_page_init=PageFactory.initElements(driver, Odrplaced_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Order_Form__2_", "TC_Order_Form__2_", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- verify Login screen");

	testReport.fillTableStep("Step 1", "verify Login screen");

	login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	login_page_init.verify_Password_Status(Step_1_Password_TEXTBOX_Status);

	login_page_init.verify_Password(Step_1_Password_TEXTBOX_Verification);
	login_page_init.verify_Login_Status(Step_1_Login_BUTTON_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Login screen");

	login_page_init.set_Username(Step_2_Username_TEXTBOX);
	login_page_init.set_Password(Step_2_Password_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_2");

	Reporter.log("Step - 3- click Login button Login screen");

	testReport.fillTableStep("Step 3", "click Login button Login screen");

	login_page_init.click_Login();
	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_3");

	Reporter.log("Step - 4- verify Account Services screen");

	testReport.fillTableStep("Step 4", "verify Account Services screen");

	account_services_page_init.verify_Open_New_Account_Status(Step_4_Open_New_Account_HYPERLINK_Status);

	account_services_page_init.verify_Transfer_Funds_Status(Step_4_Transfer_Funds_HYPERLINK_Status);

	account_services_page_init.verify_Logout_Status(Step_4_Logout_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_4");

	Reporter.log("Step - 5- verify OrderDetails screen");

	testReport.fillTableStep("Step 5", "verify OrderDetails screen");

	orderdetails_page_init.verify_OrderName_Status(Step_5_OrderName_TEXTBOX_Status);

	orderdetails_page_init.verify_OrderName(Step_5_OrderName_TEXTBOX_Verification);
	orderdetails_page_init.verify_Order_Status(Step_5_Order_HYPERLINK_Status);

	orderdetails_page_init.verify_Ok_Status(Step_5_Ok_BUTTON_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_5");

	Reporter.log("Step - 6- Fill Order form OrderDetails screen");

	testReport.fillTableStep("Step 6", "Fill Order form OrderDetails screen");

	orderdetails_page_init.set_OrderName(Step_6_OrderName_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_6");

	Reporter.log("Step - 7- click Ok button OrderDetails screen");

	testReport.fillTableStep("Step 7", "click Ok button OrderDetails screen");

	orderdetails_page_init.click_Ok();
	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_7");

	Reporter.log("Step - 8- verify Odrplaced screen");

	testReport.fillTableStep("Step 8", "verify Odrplaced screen");

	odrplaced_page_init.verify_Order_Number_Status(Step_8_Order_Number_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Order_Form__2_","Step_8");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_5");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Order_Form__2_");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
