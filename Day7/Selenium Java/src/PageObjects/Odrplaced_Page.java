package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Odrplaced_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Order_Number")
	public static WebElement Order_Number;

public void verify_Order_Number(String data){
		Assert.assertEquals(Order_Number,Order_Number);
}

public void verify_Order_Number_Status(String data){
		//Verifies the Status of the Order_Number
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Order_Number.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Order_Number.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Order_Number.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Order_Number.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}