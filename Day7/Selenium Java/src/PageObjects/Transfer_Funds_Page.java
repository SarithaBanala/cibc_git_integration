package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Transfer_Funds_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Amount")
	public static WebElement Amount;

public void verify_Amount(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Amount.getAttribute("value"),data);
	}

}

public void verify_Amount_Status(String data){
		//Verifies the Status of the Amount
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Amount.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Amount.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Amount.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Amount.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Amount(String data){
		Amount.clear();
		Amount.sendKeys(data);
}

@FindBy(how= How.ID, using = "From_Account")
	public static WebElement From_Account;

public void verify_From_Account(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(From_Account.getAttribute("value"),data);
	}

}

public void verify_From_Account_Status(String data){
		//Verifies the Status of the From_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(From_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(From_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!From_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!From_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_From_Account(String data){
		Select dropdown= new Select(From_Account);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "unnamed")
	public static WebElement unnamed;

public void verify_unnamed(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(unnamed.getAttribute("value"),data);
	}

}

public void verify_unnamed_Status(String data){
		//Verifies the Status of the unnamed
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(unnamed.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(unnamed.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!unnamed.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!unnamed.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_unnamed(String data){
		unnamed.clear();
		unnamed.sendKeys(data);
}

@FindBy(how= How.ID, using = "To_Account")
	public static WebElement To_Account;

public void verify_To_Account(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(To_Account.getAttribute("value"),data);
	}

}

public void verify_To_Account_Status(String data){
		//Verifies the Status of the To_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(To_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(To_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!To_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!To_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_To_Account(String data){
		Select dropdown= new Select(To_Account);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "TRANSFER")
	public static WebElement TRANSFER;

public void verify_TRANSFER_Status(String data){
		//Verifies the Status of the TRANSFER
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(TRANSFER.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(TRANSFER.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!TRANSFER.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!TRANSFER.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_TRANSFER(){
		TRANSFER.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Country")
	public WebElement Transfer_Funds_Country;

public void verify_Transfer_Funds_Country_Status(String data){
		//Verifies the Status of the Transfer_Funds_Country
		Assert.assertEquals(Transfer_Funds_Country,Transfer_Funds_Country);
}

public void select_Transfer_Funds_Country(){
		Transfer_Funds_Country.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Canada")
	public WebElement Transfer_Funds_Canada;

public void verify_Transfer_Funds_Canada_Status(String data){
		//Verifies the Status of the Transfer_Funds_Canada
		Assert.assertEquals(Transfer_Funds_Canada,Transfer_Funds_Canada);
}

public void select_Transfer_Funds_Canada(){
		Transfer_Funds_Canada.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_India")
	public WebElement Transfer_Funds_India;

public void verify_Transfer_Funds_India_Status(String data){
		//Verifies the Status of the Transfer_Funds_India
		Assert.assertEquals(Transfer_Funds_India,Transfer_Funds_India);
}

public void select_Transfer_Funds_India(){
		Transfer_Funds_India.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Gender")
	public WebElement Transfer_Funds_Gender;

public void verify_Transfer_Funds_Gender_Status(String data){
		//Verifies the Status of the Transfer_Funds_Gender
		Assert.assertEquals(Transfer_Funds_Gender,Transfer_Funds_Gender);
}

public void select_Transfer_Funds_Gender(){
		Transfer_Funds_Gender.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_M")
	public WebElement Transfer_Funds_M;

public void verify_Transfer_Funds_M_Status(String data){
		//Verifies the Status of the Transfer_Funds_M
		Assert.assertEquals(Transfer_Funds_M,Transfer_Funds_M);
}

public void select_Transfer_Funds_M(){
		Transfer_Funds_M.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_F")
	public WebElement Transfer_Funds_F;

public void verify_Transfer_Funds_F_Status(String data){
		//Verifies the Status of the Transfer_Funds_F
		Assert.assertEquals(Transfer_Funds_F,Transfer_Funds_F);
}

public void select_Transfer_Funds_F(){
		Transfer_Funds_F.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Account_Type")
	public WebElement Transfer_Funds_Account_Type;

public void verify_Transfer_Funds_Account_Type_Status(String data){
		//Verifies the Status of the Transfer_Funds_Account_Type
		Assert.assertEquals(Transfer_Funds_Account_Type,Transfer_Funds_Account_Type);
}

public void select_Transfer_Funds_Account_Type(){
		Transfer_Funds_Account_Type.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_CHECKINGS")
	public WebElement Transfer_Funds_CHECKINGS;

public void verify_Transfer_Funds_CHECKINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_CHECKINGS
		Assert.assertEquals(Transfer_Funds_CHECKINGS,Transfer_Funds_CHECKINGS);
}

public void select_Transfer_Funds_CHECKINGS(){
		Transfer_Funds_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_SAVINGS")
	public WebElement Transfer_Funds_SAVINGS;

public void verify_Transfer_Funds_SAVINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_SAVINGS
		Assert.assertEquals(Transfer_Funds_SAVINGS,Transfer_Funds_SAVINGS);
}

public void select_Transfer_Funds_SAVINGS(){
		Transfer_Funds_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Account")
	public WebElement Transfer_Funds_Account;

public void verify_Transfer_Funds_Account_Status(String data){
		//Verifies the Status of the Transfer_Funds_Account
		Assert.assertEquals(Transfer_Funds_Account,Transfer_Funds_Account);
}

public void select_Transfer_Funds_Account(){
		Transfer_Funds_Account.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_12345")
	public WebElement Transfer_Funds_12345;

public void verify_Transfer_Funds_12345_Status(String data){
		//Verifies the Status of the Transfer_Funds_12345
		Assert.assertEquals(Transfer_Funds_12345,Transfer_Funds_12345);
}

public void select_Transfer_Funds_12345(){
		Transfer_Funds_12345.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_98745")
	public WebElement Transfer_Funds_98745;

public void verify_Transfer_Funds_98745_Status(String data){
		//Verifies the Status of the Transfer_Funds_98745
		Assert.assertEquals(Transfer_Funds_98745,Transfer_Funds_98745);
}

public void select_Transfer_Funds_98745(){
		Transfer_Funds_98745.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Transfer_Funds")
	public WebElement Transfer_Funds_Transfer_Funds;

public void verify_Transfer_Funds_Transfer_Funds_Status(String data){
		//Verifies the Status of the Transfer_Funds_Transfer_Funds
		Assert.assertEquals(Transfer_Funds_Transfer_Funds,Transfer_Funds_Transfer_Funds);
}

public void select_Transfer_Funds_Transfer_Funds(){
		Transfer_Funds_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Country")
	public WebElement Transfer_Funds_Country;

public void verify_Transfer_Funds_Country_Status(String data){
		//Verifies the Status of the Transfer_Funds_Country
		Assert.assertEquals(Transfer_Funds_Country,Transfer_Funds_Country);
}

public void select_Transfer_Funds_Country(){
		Transfer_Funds_Country.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Canada")
	public WebElement Transfer_Funds_Canada;

public void verify_Transfer_Funds_Canada_Status(String data){
		//Verifies the Status of the Transfer_Funds_Canada
		Assert.assertEquals(Transfer_Funds_Canada,Transfer_Funds_Canada);
}

public void select_Transfer_Funds_Canada(){
		Transfer_Funds_Canada.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_India")
	public WebElement Transfer_Funds_India;

public void verify_Transfer_Funds_India_Status(String data){
		//Verifies the Status of the Transfer_Funds_India
		Assert.assertEquals(Transfer_Funds_India,Transfer_Funds_India);
}

public void select_Transfer_Funds_India(){
		Transfer_Funds_India.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Gender")
	public WebElement Transfer_Funds_Gender;

public void verify_Transfer_Funds_Gender_Status(String data){
		//Verifies the Status of the Transfer_Funds_Gender
		Assert.assertEquals(Transfer_Funds_Gender,Transfer_Funds_Gender);
}

public void select_Transfer_Funds_Gender(){
		Transfer_Funds_Gender.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_M")
	public WebElement Transfer_Funds_M;

public void verify_Transfer_Funds_M_Status(String data){
		//Verifies the Status of the Transfer_Funds_M
		Assert.assertEquals(Transfer_Funds_M,Transfer_Funds_M);
}

public void select_Transfer_Funds_M(){
		Transfer_Funds_M.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_F")
	public WebElement Transfer_Funds_F;

public void verify_Transfer_Funds_F_Status(String data){
		//Verifies the Status of the Transfer_Funds_F
		Assert.assertEquals(Transfer_Funds_F,Transfer_Funds_F);
}

public void select_Transfer_Funds_F(){
		Transfer_Funds_F.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Account_Type")
	public WebElement Transfer_Funds_Account_Type;

public void verify_Transfer_Funds_Account_Type_Status(String data){
		//Verifies the Status of the Transfer_Funds_Account_Type
		Assert.assertEquals(Transfer_Funds_Account_Type,Transfer_Funds_Account_Type);
}

public void select_Transfer_Funds_Account_Type(){
		Transfer_Funds_Account_Type.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_CHECKINGS")
	public WebElement Transfer_Funds_CHECKINGS;

public void verify_Transfer_Funds_CHECKINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_CHECKINGS
		Assert.assertEquals(Transfer_Funds_CHECKINGS,Transfer_Funds_CHECKINGS);
}

public void select_Transfer_Funds_CHECKINGS(){
		Transfer_Funds_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_SAVINGS")
	public WebElement Transfer_Funds_SAVINGS;

public void verify_Transfer_Funds_SAVINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_SAVINGS
		Assert.assertEquals(Transfer_Funds_SAVINGS,Transfer_Funds_SAVINGS);
}

public void select_Transfer_Funds_SAVINGS(){
		Transfer_Funds_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_12345")
	public WebElement Transfer_Funds_12345;

public void verify_Transfer_Funds_12345_Status(String data){
		//Verifies the Status of the Transfer_Funds_12345
		Assert.assertEquals(Transfer_Funds_12345,Transfer_Funds_12345);
}

public void select_Transfer_Funds_12345(){
		Transfer_Funds_12345.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_98745")
	public WebElement Transfer_Funds_98745;

public void verify_Transfer_Funds_98745_Status(String data){
		//Verifies the Status of the Transfer_Funds_98745
		Assert.assertEquals(Transfer_Funds_98745,Transfer_Funds_98745);
}

public void select_Transfer_Funds_98745(){
		Transfer_Funds_98745.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Transfer_Funds")
	public WebElement Transfer_Funds_Transfer_Funds;

public void verify_Transfer_Funds_Transfer_Funds_Status(String data){
		//Verifies the Status of the Transfer_Funds_Transfer_Funds
		Assert.assertEquals(Transfer_Funds_Transfer_Funds,Transfer_Funds_Transfer_Funds);
}

public void select_Transfer_Funds_Transfer_Funds(){
		Transfer_Funds_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Country")
	public WebElement Transfer_Funds_Country;

public void verify_Transfer_Funds_Country_Status(String data){
		//Verifies the Status of the Transfer_Funds_Country
		Assert.assertEquals(Transfer_Funds_Country,Transfer_Funds_Country);
}

public void select_Transfer_Funds_Country(){
		Transfer_Funds_Country.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Canada")
	public WebElement Transfer_Funds_Canada;

public void verify_Transfer_Funds_Canada_Status(String data){
		//Verifies the Status of the Transfer_Funds_Canada
		Assert.assertEquals(Transfer_Funds_Canada,Transfer_Funds_Canada);
}

public void select_Transfer_Funds_Canada(){
		Transfer_Funds_Canada.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_India")
	public WebElement Transfer_Funds_India;

public void verify_Transfer_Funds_India_Status(String data){
		//Verifies the Status of the Transfer_Funds_India
		Assert.assertEquals(Transfer_Funds_India,Transfer_Funds_India);
}

public void select_Transfer_Funds_India(){
		Transfer_Funds_India.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Gender")
	public WebElement Transfer_Funds_Gender;

public void verify_Transfer_Funds_Gender_Status(String data){
		//Verifies the Status of the Transfer_Funds_Gender
		Assert.assertEquals(Transfer_Funds_Gender,Transfer_Funds_Gender);
}

public void select_Transfer_Funds_Gender(){
		Transfer_Funds_Gender.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_M")
	public WebElement Transfer_Funds_M;

public void verify_Transfer_Funds_M_Status(String data){
		//Verifies the Status of the Transfer_Funds_M
		Assert.assertEquals(Transfer_Funds_M,Transfer_Funds_M);
}

public void select_Transfer_Funds_M(){
		Transfer_Funds_M.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_F")
	public WebElement Transfer_Funds_F;

public void verify_Transfer_Funds_F_Status(String data){
		//Verifies the Status of the Transfer_Funds_F
		Assert.assertEquals(Transfer_Funds_F,Transfer_Funds_F);
}

public void select_Transfer_Funds_F(){
		Transfer_Funds_F.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Account_Type")
	public WebElement Transfer_Funds_Account_Type;

public void verify_Transfer_Funds_Account_Type_Status(String data){
		//Verifies the Status of the Transfer_Funds_Account_Type
		Assert.assertEquals(Transfer_Funds_Account_Type,Transfer_Funds_Account_Type);
}

public void select_Transfer_Funds_Account_Type(){
		Transfer_Funds_Account_Type.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_CHECKINGS")
	public WebElement Transfer_Funds_CHECKINGS;

public void verify_Transfer_Funds_CHECKINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_CHECKINGS
		Assert.assertEquals(Transfer_Funds_CHECKINGS,Transfer_Funds_CHECKINGS);
}

public void select_Transfer_Funds_CHECKINGS(){
		Transfer_Funds_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_SAVINGS")
	public WebElement Transfer_Funds_SAVINGS;

public void verify_Transfer_Funds_SAVINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_SAVINGS
		Assert.assertEquals(Transfer_Funds_SAVINGS,Transfer_Funds_SAVINGS);
}

public void select_Transfer_Funds_SAVINGS(){
		Transfer_Funds_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Account")
	public WebElement Transfer_Funds_Account;

public void verify_Transfer_Funds_Account_Status(String data){
		//Verifies the Status of the Transfer_Funds_Account
		Assert.assertEquals(Transfer_Funds_Account,Transfer_Funds_Account);
}

public void select_Transfer_Funds_Account(){
		Transfer_Funds_Account.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_12345")
	public WebElement Transfer_Funds_12345;

public void verify_Transfer_Funds_12345_Status(String data){
		//Verifies the Status of the Transfer_Funds_12345
		Assert.assertEquals(Transfer_Funds_12345,Transfer_Funds_12345);
}

public void select_Transfer_Funds_12345(){
		Transfer_Funds_12345.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_98745")
	public WebElement Transfer_Funds_98745;

public void verify_Transfer_Funds_98745_Status(String data){
		//Verifies the Status of the Transfer_Funds_98745
		Assert.assertEquals(Transfer_Funds_98745,Transfer_Funds_98745);
}

public void select_Transfer_Funds_98745(){
		Transfer_Funds_98745.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Transfer_Funds")
	public WebElement Transfer_Funds_Transfer_Funds;

public void verify_Transfer_Funds_Transfer_Funds_Status(String data){
		//Verifies the Status of the Transfer_Funds_Transfer_Funds
		Assert.assertEquals(Transfer_Funds_Transfer_Funds,Transfer_Funds_Transfer_Funds);
}

public void select_Transfer_Funds_Transfer_Funds(){
		Transfer_Funds_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Country")
	public WebElement Transfer_Funds_Country;

public void verify_Transfer_Funds_Country_Status(String data){
		//Verifies the Status of the Transfer_Funds_Country
		Assert.assertEquals(Transfer_Funds_Country,Transfer_Funds_Country);
}

public void select_Transfer_Funds_Country(){
		Transfer_Funds_Country.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Canada")
	public WebElement Transfer_Funds_Canada;

public void verify_Transfer_Funds_Canada_Status(String data){
		//Verifies the Status of the Transfer_Funds_Canada
		Assert.assertEquals(Transfer_Funds_Canada,Transfer_Funds_Canada);
}

public void select_Transfer_Funds_Canada(){
		Transfer_Funds_Canada.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_India")
	public WebElement Transfer_Funds_India;

public void verify_Transfer_Funds_India_Status(String data){
		//Verifies the Status of the Transfer_Funds_India
		Assert.assertEquals(Transfer_Funds_India,Transfer_Funds_India);
}

public void select_Transfer_Funds_India(){
		Transfer_Funds_India.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Gender")
	public WebElement Transfer_Funds_Gender;

public void verify_Transfer_Funds_Gender_Status(String data){
		//Verifies the Status of the Transfer_Funds_Gender
		Assert.assertEquals(Transfer_Funds_Gender,Transfer_Funds_Gender);
}

public void select_Transfer_Funds_Gender(){
		Transfer_Funds_Gender.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_M")
	public WebElement Transfer_Funds_M;

public void verify_Transfer_Funds_M_Status(String data){
		//Verifies the Status of the Transfer_Funds_M
		Assert.assertEquals(Transfer_Funds_M,Transfer_Funds_M);
}

public void select_Transfer_Funds_M(){
		Transfer_Funds_M.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_F")
	public WebElement Transfer_Funds_F;

public void verify_Transfer_Funds_F_Status(String data){
		//Verifies the Status of the Transfer_Funds_F
		Assert.assertEquals(Transfer_Funds_F,Transfer_Funds_F);
}

public void select_Transfer_Funds_F(){
		Transfer_Funds_F.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Account_Type")
	public WebElement Transfer_Funds_Account_Type;

public void verify_Transfer_Funds_Account_Type_Status(String data){
		//Verifies the Status of the Transfer_Funds_Account_Type
		Assert.assertEquals(Transfer_Funds_Account_Type,Transfer_Funds_Account_Type);
}

public void select_Transfer_Funds_Account_Type(){
		Transfer_Funds_Account_Type.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_CHECKINGS")
	public WebElement Transfer_Funds_CHECKINGS;

public void verify_Transfer_Funds_CHECKINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_CHECKINGS
		Assert.assertEquals(Transfer_Funds_CHECKINGS,Transfer_Funds_CHECKINGS);
}

public void select_Transfer_Funds_CHECKINGS(){
		Transfer_Funds_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_SAVINGS")
	public WebElement Transfer_Funds_SAVINGS;

public void verify_Transfer_Funds_SAVINGS_Status(String data){
		//Verifies the Status of the Transfer_Funds_SAVINGS
		Assert.assertEquals(Transfer_Funds_SAVINGS,Transfer_Funds_SAVINGS);
}

public void select_Transfer_Funds_SAVINGS(){
		Transfer_Funds_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_12345")
	public WebElement Transfer_Funds_12345;

public void verify_Transfer_Funds_12345_Status(String data){
		//Verifies the Status of the Transfer_Funds_12345
		Assert.assertEquals(Transfer_Funds_12345,Transfer_Funds_12345);
}

public void select_Transfer_Funds_12345(){
		Transfer_Funds_12345.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_98745")
	public WebElement Transfer_Funds_98745;

public void verify_Transfer_Funds_98745_Status(String data){
		//Verifies the Status of the Transfer_Funds_98745
		Assert.assertEquals(Transfer_Funds_98745,Transfer_Funds_98745);
}

public void select_Transfer_Funds_98745(){
		Transfer_Funds_98745.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_Transfer_Funds")
	public WebElement Transfer_Funds_Transfer_Funds;

public void verify_Transfer_Funds_Transfer_Funds_Status(String data){
		//Verifies the Status of the Transfer_Funds_Transfer_Funds
		Assert.assertEquals(Transfer_Funds_Transfer_Funds,Transfer_Funds_Transfer_Funds);
}

public void select_Transfer_Funds_Transfer_Funds(){
		Transfer_Funds_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds_unnamed")
	public WebElement Transfer_Funds_unnamed;

public void verify_Transfer_Funds_unnamed_Status(String data){
		//Verifies the Status of the Transfer_Funds_unnamed
		Assert.assertEquals(Transfer_Funds_unnamed,Transfer_Funds_unnamed);
}

public void select_Transfer_Funds_unnamed(){
		Transfer_Funds_unnamed.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}