package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Screen1_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "HyperLink")
	public static WebElement HyperLink;

public void verify_HyperLink_Status(String data){
		//Verifies the Status of the HyperLink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(HyperLink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(HyperLink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!HyperLink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!HyperLink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_HyperLink(){
		HyperLink.click();
}

@FindBy(how= How.ID, using = "FACalldate")
	public static WebElement FACalldate;

public void verify_FACalldate(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(FACalldate.getAttribute("value"),data);
	}

}

public void verify_FACalldate_Status(String data){
		//Verifies the Status of the FACalldate
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(FACalldate.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(FACalldate.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!FACalldate.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!FACalldate.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_FACalldate(String data){
		FACalldate.clear();
		FACalldate.sendKeys(data);
}

@FindBy(how= How.ID, using = "FSCallDate")
	public static WebElement FSCallDate;

public void verify_FSCallDate(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(FSCallDate.getAttribute("value"),data);
	}

}

public void verify_FSCallDate_Status(String data){
		//Verifies the Status of the FSCallDate
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(FSCallDate.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(FSCallDate.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!FSCallDate.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!FSCallDate.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_FSCallDate(String data){
		FSCallDate.clear();
		FSCallDate.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}