package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class S1_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Next")
	public static WebElement Next;

public void verify_Next_Status(String data){
		//Verifies the Status of the Next
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Next.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Next.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Next.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Next.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Next(){
		Next.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}