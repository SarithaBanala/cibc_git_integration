package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Open_New_Account_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Account_Type")
	public static WebElement Account_Type;

public void verify_Account_Type(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Account_Type.getAttribute("value"),data);
	}

}

public void verify_Account_Type_Status(String data){
		//Verifies the Status of the Account_Type
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Type.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Type.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Type.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Type.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Account_Type(String data){
		Select dropdown= new Select(Account_Type);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Excisting_account")
	public static WebElement Excisting_account;

public void verify_Excisting_account(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Excisting_account.getAttribute("value"),data);
	}

}

public void verify_Excisting_account_Status(String data){
		//Verifies the Status of the Excisting_account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Excisting_account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Excisting_account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Excisting_account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Excisting_account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Excisting_account(String data){
		Select dropdown= new Select(Excisting_account);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "OPEN_NEW_ACCOUNT")
	public static WebElement OPEN_NEW_ACCOUNT;

public void verify_OPEN_NEW_ACCOUNT_Status(String data){
		//Verifies the Status of the OPEN_NEW_ACCOUNT
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(OPEN_NEW_ACCOUNT.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(OPEN_NEW_ACCOUNT.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!OPEN_NEW_ACCOUNT.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!OPEN_NEW_ACCOUNT.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_OPEN_NEW_ACCOUNT(){
		OPEN_NEW_ACCOUNT.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Country")
	public WebElement Open_New_Account_Country;

public void verify_Open_New_Account_Country_Status(String data){
		//Verifies the Status of the Open_New_Account_Country
		Assert.assertEquals(Open_New_Account_Country,Open_New_Account_Country);
}

public void select_Open_New_Account_Country(){
		Open_New_Account_Country.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Canada")
	public WebElement Open_New_Account_Canada;

public void verify_Open_New_Account_Canada_Status(String data){
		//Verifies the Status of the Open_New_Account_Canada
		Assert.assertEquals(Open_New_Account_Canada,Open_New_Account_Canada);
}

public void select_Open_New_Account_Canada(){
		Open_New_Account_Canada.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_India")
	public WebElement Open_New_Account_India;

public void verify_Open_New_Account_India_Status(String data){
		//Verifies the Status of the Open_New_Account_India
		Assert.assertEquals(Open_New_Account_India,Open_New_Account_India);
}

public void select_Open_New_Account_India(){
		Open_New_Account_India.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Gender")
	public WebElement Open_New_Account_Gender;

public void verify_Open_New_Account_Gender_Status(String data){
		//Verifies the Status of the Open_New_Account_Gender
		Assert.assertEquals(Open_New_Account_Gender,Open_New_Account_Gender);
}

public void select_Open_New_Account_Gender(){
		Open_New_Account_Gender.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_M")
	public WebElement Open_New_Account_M;

public void verify_Open_New_Account_M_Status(String data){
		//Verifies the Status of the Open_New_Account_M
		Assert.assertEquals(Open_New_Account_M,Open_New_Account_M);
}

public void select_Open_New_Account_M(){
		Open_New_Account_M.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_F")
	public WebElement Open_New_Account_F;

public void verify_Open_New_Account_F_Status(String data){
		//Verifies the Status of the Open_New_Account_F
		Assert.assertEquals(Open_New_Account_F,Open_New_Account_F);
}

public void select_Open_New_Account_F(){
		Open_New_Account_F.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_unnamed")
	public WebElement Open_New_Account_unnamed;

public void verify_Open_New_Account_unnamed_Status(String data){
		//Verifies the Status of the Open_New_Account_unnamed
		Assert.assertEquals(Open_New_Account_unnamed,Open_New_Account_unnamed);
}

public void select_Open_New_Account_unnamed(){
		Open_New_Account_unnamed.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_CHECKINGS")
	public WebElement Open_New_Account_CHECKINGS;

public void verify_Open_New_Account_CHECKINGS_Status(String data){
		//Verifies the Status of the Open_New_Account_CHECKINGS
		Assert.assertEquals(Open_New_Account_CHECKINGS,Open_New_Account_CHECKINGS);
}

public void select_Open_New_Account_CHECKINGS(){
		Open_New_Account_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_SAVINGS")
	public WebElement Open_New_Account_SAVINGS;

public void verify_Open_New_Account_SAVINGS_Status(String data){
		//Verifies the Status of the Open_New_Account_SAVINGS
		Assert.assertEquals(Open_New_Account_SAVINGS,Open_New_Account_SAVINGS);
}

public void select_Open_New_Account_SAVINGS(){
		Open_New_Account_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Account")
	public WebElement Open_New_Account_Account;

public void verify_Open_New_Account_Account_Status(String data){
		//Verifies the Status of the Open_New_Account_Account
		Assert.assertEquals(Open_New_Account_Account,Open_New_Account_Account);
}

public void select_Open_New_Account_Account(){
		Open_New_Account_Account.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_12345")
	public WebElement Open_New_Account_12345;

public void verify_Open_New_Account_12345_Status(String data){
		//Verifies the Status of the Open_New_Account_12345
		Assert.assertEquals(Open_New_Account_12345,Open_New_Account_12345);
}

public void select_Open_New_Account_12345(){
		Open_New_Account_12345.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_98745")
	public WebElement Open_New_Account_98745;

public void verify_Open_New_Account_98745_Status(String data){
		//Verifies the Status of the Open_New_Account_98745
		Assert.assertEquals(Open_New_Account_98745,Open_New_Account_98745);
}

public void select_Open_New_Account_98745(){
		Open_New_Account_98745.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Transfer_Funds")
	public WebElement Open_New_Account_Transfer_Funds;

public void verify_Open_New_Account_Transfer_Funds_Status(String data){
		//Verifies the Status of the Open_New_Account_Transfer_Funds
		Assert.assertEquals(Open_New_Account_Transfer_Funds,Open_New_Account_Transfer_Funds);
}

public void select_Open_New_Account_Transfer_Funds(){
		Open_New_Account_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_unnamed")
	public WebElement Open_New_Account_unnamed;

public void verify_Open_New_Account_unnamed_Status(String data){
		//Verifies the Status of the Open_New_Account_unnamed
		Assert.assertEquals(Open_New_Account_unnamed,Open_New_Account_unnamed);
}

public void select_Open_New_Account_unnamed(){
		Open_New_Account_unnamed.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Country")
	public WebElement Open_New_Account_Country;

public void verify_Open_New_Account_Country_Status(String data){
		//Verifies the Status of the Open_New_Account_Country
		Assert.assertEquals(Open_New_Account_Country,Open_New_Account_Country);
}

public void select_Open_New_Account_Country(){
		Open_New_Account_Country.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Canada")
	public WebElement Open_New_Account_Canada;

public void verify_Open_New_Account_Canada_Status(String data){
		//Verifies the Status of the Open_New_Account_Canada
		Assert.assertEquals(Open_New_Account_Canada,Open_New_Account_Canada);
}

public void select_Open_New_Account_Canada(){
		Open_New_Account_Canada.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_India")
	public WebElement Open_New_Account_India;

public void verify_Open_New_Account_India_Status(String data){
		//Verifies the Status of the Open_New_Account_India
		Assert.assertEquals(Open_New_Account_India,Open_New_Account_India);
}

public void select_Open_New_Account_India(){
		Open_New_Account_India.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Gender")
	public WebElement Open_New_Account_Gender;

public void verify_Open_New_Account_Gender_Status(String data){
		//Verifies the Status of the Open_New_Account_Gender
		Assert.assertEquals(Open_New_Account_Gender,Open_New_Account_Gender);
}

public void select_Open_New_Account_Gender(){
		Open_New_Account_Gender.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_M")
	public WebElement Open_New_Account_M;

public void verify_Open_New_Account_M_Status(String data){
		//Verifies the Status of the Open_New_Account_M
		Assert.assertEquals(Open_New_Account_M,Open_New_Account_M);
}

public void select_Open_New_Account_M(){
		Open_New_Account_M.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_F")
	public WebElement Open_New_Account_F;

public void verify_Open_New_Account_F_Status(String data){
		//Verifies the Status of the Open_New_Account_F
		Assert.assertEquals(Open_New_Account_F,Open_New_Account_F);
}

public void select_Open_New_Account_F(){
		Open_New_Account_F.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_unnamed")
	public WebElement Open_New_Account_unnamed;

public void verify_Open_New_Account_unnamed_Status(String data){
		//Verifies the Status of the Open_New_Account_unnamed
		Assert.assertEquals(Open_New_Account_unnamed,Open_New_Account_unnamed);
}

public void select_Open_New_Account_unnamed(){
		Open_New_Account_unnamed.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Account_Type")
	public WebElement Open_New_Account_Account_Type;

public void verify_Open_New_Account_Account_Type_Status(String data){
		//Verifies the Status of the Open_New_Account_Account_Type
		Assert.assertEquals(Open_New_Account_Account_Type,Open_New_Account_Account_Type);
}

public void select_Open_New_Account_Account_Type(){
		Open_New_Account_Account_Type.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_CHECKINGS")
	public WebElement Open_New_Account_CHECKINGS;

public void verify_Open_New_Account_CHECKINGS_Status(String data){
		//Verifies the Status of the Open_New_Account_CHECKINGS
		Assert.assertEquals(Open_New_Account_CHECKINGS,Open_New_Account_CHECKINGS);
}

public void select_Open_New_Account_CHECKINGS(){
		Open_New_Account_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_SAVINGS")
	public WebElement Open_New_Account_SAVINGS;

public void verify_Open_New_Account_SAVINGS_Status(String data){
		//Verifies the Status of the Open_New_Account_SAVINGS
		Assert.assertEquals(Open_New_Account_SAVINGS,Open_New_Account_SAVINGS);
}

public void select_Open_New_Account_SAVINGS(){
		Open_New_Account_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_12345")
	public WebElement Open_New_Account_12345;

public void verify_Open_New_Account_12345_Status(String data){
		//Verifies the Status of the Open_New_Account_12345
		Assert.assertEquals(Open_New_Account_12345,Open_New_Account_12345);
}

public void select_Open_New_Account_12345(){
		Open_New_Account_12345.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_98745")
	public WebElement Open_New_Account_98745;

public void verify_Open_New_Account_98745_Status(String data){
		//Verifies the Status of the Open_New_Account_98745
		Assert.assertEquals(Open_New_Account_98745,Open_New_Account_98745);
}

public void select_Open_New_Account_98745(){
		Open_New_Account_98745.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_Transfer_Funds")
	public WebElement Open_New_Account_Transfer_Funds;

public void verify_Open_New_Account_Transfer_Funds_Status(String data){
		//Verifies the Status of the Open_New_Account_Transfer_Funds
		Assert.assertEquals(Open_New_Account_Transfer_Funds,Open_New_Account_Transfer_Funds);
}

public void select_Open_New_Account_Transfer_Funds(){
		Open_New_Account_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Open_New_Account_unnamed")
	public WebElement Open_New_Account_unnamed;

public void verify_Open_New_Account_unnamed_Status(String data){
		//Verifies the Status of the Open_New_Account_unnamed
		Assert.assertEquals(Open_New_Account_unnamed,Open_New_Account_unnamed);
}

public void select_Open_New_Account_unnamed(){
		Open_New_Account_unnamed.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}