package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class RightClickOptions_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Open_Link")
	public static WebElement Open_Link;

public void verify_Open_Link_Status(String data){
		//Verifies the Status of the Open_Link
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Open_Link.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Open_Link.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Open_Link.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Open_Link.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Open_Link(){
		Open_Link.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}