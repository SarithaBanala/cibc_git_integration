package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Login_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Username")
	public static WebElement Username;

public void verify_Username(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Username.getAttribute("value"),data);
	}

}

public void verify_Username_Status(String data){
		//Verifies the Status of the Username
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Username.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Username.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Username.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Username.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Username(String data){
		Username.clear();
		Username.sendKeys(data);
}

@FindBy(how= How.ID, using = "Password")
	public static WebElement Password;

public void verify_Password(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Password.getAttribute("value"),data);
	}

}

public void verify_Password_Status(String data){
		//Verifies the Status of the Password
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Password.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Password.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Password.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Password.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Password(String data){
		Password.clear();
		Password.sendKeys(data);
}

@FindBy(how= How.ID, using = "Login")
	public static WebElement Login;

public void verify_Login_Status(String data){
		//Verifies the Status of the Login
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Login.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Login.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Login.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Login.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Login(){
		Login.click();
}

@FindBy(how= How.ID, using = "Mobile_Number")
	public static WebElement Mobile_Number;

public void verify_Mobile_Number(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Mobile_Number.getAttribute("value"),data);
	}

}

public void verify_Mobile_Number_Status(String data){
		//Verifies the Status of the Mobile_Number
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Mobile_Number.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Mobile_Number.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Mobile_Number.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Mobile_Number.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Mobile_Number(String data){
		Mobile_Number.clear();
		Mobile_Number.sendKeys(data);
}

@FindBy(how= How.ID, using = "I_agree")
	public static WebElement I_agree;

public void verify_I_agree(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(I_agree.getAttribute("value"),data);
	}

}

public void verify_I_agree_Status(String data){
		//Verifies the Status of the I_agree
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(I_agree.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(I_agree.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!I_agree.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!I_agree.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_I_agree(String data){
		if(I_agree.isSelected());
			I_agree.click();
}

@FindBy(how= How.ID, using = "Email")
	public static WebElement Email;

public void verify_Email(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Email.getAttribute("value"),data);
	}

}

public void verify_Email_Status(String data){
		//Verifies the Status of the Email
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Email.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Email.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Email.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Email.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Email(String data){
		Email.clear();
		Email.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}