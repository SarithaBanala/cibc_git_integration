package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class DisplayMessage_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Verify")
	public static WebElement Verify;

public void verify_Verify(String data){
		Assert.assertEquals(Verify,Verify);
}

public void enter_Verify(String data){
		Verify.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}