package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Screen_Widget_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "TB")
	public static WebElement TB;

public void verify_TB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(TB.getAttribute("value"),data);
	}

}

public void verify_TB_Status(String data){
		//Verifies the Status of the TB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(TB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(TB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!TB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!TB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_TB(String data){
		TB.clear();
		TB.sendKeys(data);
}

@FindBy(how= How.ID, using = "CB")
	public static WebElement CB;

public void verify_CB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(CB.getAttribute("value"),data);
	}

}

public void verify_CB_Status(String data){
		//Verifies the Status of the CB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(CB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(CB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!CB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!CB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_CB(String data){
		if(CB.isSelected());
			CB.click();
}

@FindBy(how= How.ID, using = "DD")
	public static WebElement DD;

public void verify_DD(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(DD.getAttribute("value"),data);
	}

}

public void verify_DD_Status(String data){
		//Verifies the Status of the DD
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(DD.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(DD.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!DD.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!DD.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_DD(String data){
		Select dropdown= new Select(DD);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "")
	public static WebElement RB;

public void verify_RB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(RB.getAttribute("name"),data);
	}

}

public void verify_RB_Status(String data){
		//Verifies the Status of the RB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(RB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(RB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!RB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!RB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Calendar")
	public static WebElement Calendar;

public void verify_Calendar(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Calendar.getAttribute("value"),data);
	}

}

public void verify_Calendar_Status(String data){
		//Verifies the Status of the Calendar
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Calendar.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Calendar.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Calendar.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Calendar.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Calendar(String data){
		Calendar.click();
}

@FindBy(how= How.ID, using = "LB")
	public static WebElement LB;

public void verify_LB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(LB.getAttribute("value"),data);
	}

}

public void verify_LB_Status(String data){
		//Verifies the Status of the LB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(LB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(LB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!LB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!LB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_LB(String data){
		LB.click();
}

@FindBy(how= How.ID, using = "Btn1")
	public static WebElement Btn1;

public void verify_Btn1_Status(String data){
		//Verifies the Status of the Btn1
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Btn1.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Btn1.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Btn1.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Btn1.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Btn1(){
		Btn1.click();
}

@FindBy(how= How.ID, using = "HyperLink")
	public static WebElement HyperLink;

public void verify_HyperLink_Status(String data){
		//Verifies the Status of the HyperLink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(HyperLink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(HyperLink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!HyperLink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!HyperLink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_HyperLink(){
		HyperLink.click();
}

@FindBy(how= How.ID, using = "Message")
	public static WebElement Message;

public void verify_Message(String data){
		Assert.assertEquals(Message,Message);
}

public void verify_Message_Status(String data){
		//Verifies the Status of the Message
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Message.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Message.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Message.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Message.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Menu")
	public static WebElement Menu;

public void click_Menu(){
		Menu.click();
}

@FindBy(how= How.ID, using = "File")
	public static WebElement File;

public void click_File(){
		File.click();
}

@FindBy(how= How.ID, using = "ClickOption")
	public static WebElement ClickOption;

public void click_ClickOption(){
		ClickOption.click();
}

@FindBy(how= How.ID, using = "Mousehover")
	public static WebElement Mousehover;

public void click_Mousehover(){
		Mousehover.click();
}

@FindBy(how= How.ID, using = "SubMenu")
	public static WebElement SubMenu;

public void click_SubMenu(){
		SubMenu.click();
}

@FindBy(how= How.ID, using = "Modeling")
	public static WebElement Modeling;

public void click_Modeling(){
		Modeling.click();
}

@FindBy(how= How.ID, using = "Test_Design")
	public static WebElement Test_Design;

public void click_Test_Design(){
		Test_Design.click();
}

@FindBy(how= How.ID, using = "Group")
	public static WebElement Group;

public void click_Group(){
		Group.click();
}

@FindBy(how= How.ID, using = "Day1")
	public static WebElement Day1;

public void click_Day1(){
		Day1.click();
}

@FindBy(how= How.ID, using = "DC")
	public static WebElement DC;

public void click_DC(){
		DC.click();
}

@FindBy(how= How.ID, using = "ExcelScripter")
	public static WebElement ExcelScripter;

public void click_ExcelScripter(){
		ExcelScripter.click();
}

@FindBy(how= How.ID, using = "ConformiQ_Options")
	public static WebElement ConformiQ_Options;

public void click_ConformiQ_Options(){
		ConformiQ_Options.click();
}

@FindBy(how= How.ID, using = "Requirement_Providers")
	public static WebElement Requirement_Providers;

public void click_Requirement_Providers(){
		Requirement_Providers.click();
}

@FindBy(how= How.ID, using = "unnamed")
	public static WebElement unnamed;

public void click_unnamed(){
		unnamed.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_Country")
	public WebElement selenium_______;_selenium_______Country;

public void verify_selenium_______;_selenium_______Country_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______Country
		Assert.assertEquals(selenium_______;_selenium_______Country,selenium_______;_selenium_______Country);
}

public void select_selenium_______;_selenium_______Country(){
		selenium_______;_selenium_______Country.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_Canada")
	public WebElement selenium_______;_selenium_______Canada;

public void verify_selenium_______;_selenium_______Canada_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______Canada
		Assert.assertEquals(selenium_______;_selenium_______Canada,selenium_______;_selenium_______Canada);
}

public void select_selenium_______;_selenium_______Canada(){
		selenium_______;_selenium_______Canada.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_India")
	public WebElement selenium_______;_selenium_______India;

public void verify_selenium_______;_selenium_______India_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______India
		Assert.assertEquals(selenium_______;_selenium_______India,selenium_______;_selenium_______India);
}

public void select_selenium_______;_selenium_______India(){
		selenium_______;_selenium_______India.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_M")
	public WebElement selenium_______;_selenium_______M;

public void verify_selenium_______;_selenium_______M_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______M
		Assert.assertEquals(selenium_______;_selenium_______M,selenium_______;_selenium_______M);
}

public void select_selenium_______;_selenium_______M(){
		selenium_______;_selenium_______M.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_F")
	public WebElement selenium_______;_selenium_______F;

public void verify_selenium_______;_selenium_______F_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______F
		Assert.assertEquals(selenium_______;_selenium_______F,selenium_______;_selenium_______F);
}

public void select_selenium_______;_selenium_______F(){
		selenium_______;_selenium_______F.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_unnamed")
	public WebElement selenium_______;_selenium_______unnamed;

public void verify_selenium_______;_selenium_______unnamed_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______unnamed
		Assert.assertEquals(selenium_______;_selenium_______unnamed,selenium_______;_selenium_______unnamed);
}

public void select_selenium_______;_selenium_______unnamed(){
		selenium_______;_selenium_______unnamed.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_Account_Type")
	public WebElement selenium_______;_selenium_______Account_Type;

public void verify_selenium_______;_selenium_______Account_Type_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______Account_Type
		Assert.assertEquals(selenium_______;_selenium_______Account_Type,selenium_______;_selenium_______Account_Type);
}

public void select_selenium_______;_selenium_______Account_Type(){
		selenium_______;_selenium_______Account_Type.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_CHECKINGS")
	public WebElement selenium_______;_selenium_______CHECKINGS;

public void verify_selenium_______;_selenium_______CHECKINGS_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______CHECKINGS
		Assert.assertEquals(selenium_______;_selenium_______CHECKINGS,selenium_______;_selenium_______CHECKINGS);
}

public void select_selenium_______;_selenium_______CHECKINGS(){
		selenium_______;_selenium_______CHECKINGS.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_SAVINGS")
	public WebElement selenium_______;_selenium_______SAVINGS;

public void verify_selenium_______;_selenium_______SAVINGS_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______SAVINGS
		Assert.assertEquals(selenium_______;_selenium_______SAVINGS,selenium_______;_selenium_______SAVINGS);
}

public void select_selenium_______;_selenium_______SAVINGS(){
		selenium_______;_selenium_______SAVINGS.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_Account")
	public WebElement selenium_______;_selenium_______Account;

public void verify_selenium_______;_selenium_______Account_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______Account
		Assert.assertEquals(selenium_______;_selenium_______Account,selenium_______;_selenium_______Account);
}

public void select_selenium_______;_selenium_______Account(){
		selenium_______;_selenium_______Account.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_12345")
	public WebElement selenium_______;_selenium_______12345;

public void verify_selenium_______;_selenium_______12345_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______12345
		Assert.assertEquals(selenium_______;_selenium_______12345,selenium_______;_selenium_______12345);
}

public void select_selenium_______;_selenium_______12345(){
		selenium_______;_selenium_______12345.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_98745")
	public WebElement selenium_______;_selenium_______98745;

public void verify_selenium_______;_selenium_______98745_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______98745
		Assert.assertEquals(selenium_______;_selenium_______98745,selenium_______;_selenium_______98745);
}

public void select_selenium_______;_selenium_______98745(){
		selenium_______;_selenium_______98745.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_Transfer_Funds")
	public WebElement selenium_______;_selenium_______Transfer_Funds;

public void verify_selenium_______;_selenium_______Transfer_Funds_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______Transfer_Funds
		Assert.assertEquals(selenium_______;_selenium_______Transfer_Funds,selenium_______;_selenium_______Transfer_Funds);
}

public void select_selenium_______;_selenium_______Transfer_Funds(){
		selenium_______;_selenium_______Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"";"selenium:"_=_"_unnamed")
	public WebElement selenium_______;_selenium_______unnamed;

public void verify_selenium_______;_selenium_______unnamed_Status(String data){
		//Verifies the Status of the selenium_______;_selenium_______unnamed
		Assert.assertEquals(selenium_______;_selenium_______unnamed,selenium_______;_selenium_______unnamed);
}

public void select_selenium_______;_selenium_______unnamed(){
		selenium_______;_selenium_______unnamed.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}