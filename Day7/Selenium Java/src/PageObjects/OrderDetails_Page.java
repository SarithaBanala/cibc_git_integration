package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class OrderDetails_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "OrderName")
	public static WebElement OrderName;

public void verify_OrderName(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(OrderName.getAttribute("value"),data);
	}

}

public void verify_OrderName_Status(String data){
		//Verifies the Status of the OrderName
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(OrderName.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(OrderName.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!OrderName.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!OrderName.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_OrderName(String data){
		OrderName.clear();
		OrderName.sendKeys(data);
}

@FindBy(how= How.ID, using = "Qty")
	public static WebElement Qty;

public void verify_Qty(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Qty.getAttribute("value"),data);
	}

}

public void verify_Qty_Status(String data){
		//Verifies the Status of the Qty
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Qty.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Qty.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Qty.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Qty.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Qty(String data){
		Qty.clear();
		Qty.sendKeys(data);
}

@FindBy(how= How.ID, using = "Order")
	public static WebElement Order;

public void verify_Order_Status(String data){
		//Verifies the Status of the Order
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Order.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Order.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Order.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Order.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Order(){
		Order.click();
}

@FindBy(how= How.ID, using = "Ok")
	public static WebElement Ok;

public void verify_Ok_Status(String data){
		//Verifies the Status of the Ok
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Ok.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Ok.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Ok.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Ok.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Ok(){
		Ok.click();
}

@FindBy(how= How.ID, using = "Order_Country")
	public WebElement Order_Country;

public void verify_Order_Country_Status(String data){
		//Verifies the Status of the Order_Country
		Assert.assertEquals(Order_Country,Order_Country);
}

public void select_Order_Country(){
		Order_Country.click();
}

@FindBy(how= How.ID, using = "Order_Canada")
	public WebElement Order_Canada;

public void verify_Order_Canada_Status(String data){
		//Verifies the Status of the Order_Canada
		Assert.assertEquals(Order_Canada,Order_Canada);
}

public void select_Order_Canada(){
		Order_Canada.click();
}

@FindBy(how= How.ID, using = "Order_India")
	public WebElement Order_India;

public void verify_Order_India_Status(String data){
		//Verifies the Status of the Order_India
		Assert.assertEquals(Order_India,Order_India);
}

public void select_Order_India(){
		Order_India.click();
}

@FindBy(how= How.ID, using = "Order_Gender")
	public WebElement Order_Gender;

public void verify_Order_Gender_Status(String data){
		//Verifies the Status of the Order_Gender
		Assert.assertEquals(Order_Gender,Order_Gender);
}

public void select_Order_Gender(){
		Order_Gender.click();
}

@FindBy(how= How.ID, using = "Order_M")
	public WebElement Order_M;

public void verify_Order_M_Status(String data){
		//Verifies the Status of the Order_M
		Assert.assertEquals(Order_M,Order_M);
}

public void select_Order_M(){
		Order_M.click();
}

@FindBy(how= How.ID, using = "Order_F")
	public WebElement Order_F;

public void verify_Order_F_Status(String data){
		//Verifies the Status of the Order_F
		Assert.assertEquals(Order_F,Order_F);
}

public void select_Order_F(){
		Order_F.click();
}

@FindBy(how= How.ID, using = "Order_unnamed")
	public WebElement Order_unnamed;

public void verify_Order_unnamed_Status(String data){
		//Verifies the Status of the Order_unnamed
		Assert.assertEquals(Order_unnamed,Order_unnamed);
}

public void select_Order_unnamed(){
		Order_unnamed.click();
}

@FindBy(how= How.ID, using = "Order_Account_Type")
	public WebElement Order_Account_Type;

public void verify_Order_Account_Type_Status(String data){
		//Verifies the Status of the Order_Account_Type
		Assert.assertEquals(Order_Account_Type,Order_Account_Type);
}

public void select_Order_Account_Type(){
		Order_Account_Type.click();
}

@FindBy(how= How.ID, using = "Order_CHECKINGS")
	public WebElement Order_CHECKINGS;

public void verify_Order_CHECKINGS_Status(String data){
		//Verifies the Status of the Order_CHECKINGS
		Assert.assertEquals(Order_CHECKINGS,Order_CHECKINGS);
}

public void select_Order_CHECKINGS(){
		Order_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Order_SAVINGS")
	public WebElement Order_SAVINGS;

public void verify_Order_SAVINGS_Status(String data){
		//Verifies the Status of the Order_SAVINGS
		Assert.assertEquals(Order_SAVINGS,Order_SAVINGS);
}

public void select_Order_SAVINGS(){
		Order_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Order_Account")
	public WebElement Order_Account;

public void verify_Order_Account_Status(String data){
		//Verifies the Status of the Order_Account
		Assert.assertEquals(Order_Account,Order_Account);
}

public void select_Order_Account(){
		Order_Account.click();
}

@FindBy(how= How.ID, using = "Order_12345")
	public WebElement Order_12345;

public void verify_Order_12345_Status(String data){
		//Verifies the Status of the Order_12345
		Assert.assertEquals(Order_12345,Order_12345);
}

public void select_Order_12345(){
		Order_12345.click();
}

@FindBy(how= How.ID, using = "Order_98745")
	public WebElement Order_98745;

public void verify_Order_98745_Status(String data){
		//Verifies the Status of the Order_98745
		Assert.assertEquals(Order_98745,Order_98745);
}

public void select_Order_98745(){
		Order_98745.click();
}

@FindBy(how= How.ID, using = "Order_Transfer_Funds")
	public WebElement Order_Transfer_Funds;

public void verify_Order_Transfer_Funds_Status(String data){
		//Verifies the Status of the Order_Transfer_Funds
		Assert.assertEquals(Order_Transfer_Funds,Order_Transfer_Funds);
}

public void select_Order_Transfer_Funds(){
		Order_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Order_unnamed")
	public WebElement Order_unnamed;

public void verify_Order_unnamed_Status(String data){
		//Verifies the Status of the Order_unnamed
		Assert.assertEquals(Order_unnamed,Order_unnamed);
}

public void select_Order_unnamed(){
		Order_unnamed.click();
}

@FindBy(how= How.ID, using = "Order_Country")
	public WebElement Order_Country;

public void verify_Order_Country_Status(String data){
		//Verifies the Status of the Order_Country
		Assert.assertEquals(Order_Country,Order_Country);
}

public void select_Order_Country(){
		Order_Country.click();
}

@FindBy(how= How.ID, using = "Order_Canada")
	public WebElement Order_Canada;

public void verify_Order_Canada_Status(String data){
		//Verifies the Status of the Order_Canada
		Assert.assertEquals(Order_Canada,Order_Canada);
}

public void select_Order_Canada(){
		Order_Canada.click();
}

@FindBy(how= How.ID, using = "Order_India")
	public WebElement Order_India;

public void verify_Order_India_Status(String data){
		//Verifies the Status of the Order_India
		Assert.assertEquals(Order_India,Order_India);
}

public void select_Order_India(){
		Order_India.click();
}

@FindBy(how= How.ID, using = "Order_Gender")
	public WebElement Order_Gender;

public void verify_Order_Gender_Status(String data){
		//Verifies the Status of the Order_Gender
		Assert.assertEquals(Order_Gender,Order_Gender);
}

public void select_Order_Gender(){
		Order_Gender.click();
}

@FindBy(how= How.ID, using = "Order_M")
	public WebElement Order_M;

public void verify_Order_M_Status(String data){
		//Verifies the Status of the Order_M
		Assert.assertEquals(Order_M,Order_M);
}

public void select_Order_M(){
		Order_M.click();
}

@FindBy(how= How.ID, using = "Order_F")
	public WebElement Order_F;

public void verify_Order_F_Status(String data){
		//Verifies the Status of the Order_F
		Assert.assertEquals(Order_F,Order_F);
}

public void select_Order_F(){
		Order_F.click();
}

@FindBy(how= How.ID, using = "Order_unnamed")
	public WebElement Order_unnamed;

public void verify_Order_unnamed_Status(String data){
		//Verifies the Status of the Order_unnamed
		Assert.assertEquals(Order_unnamed,Order_unnamed);
}

public void select_Order_unnamed(){
		Order_unnamed.click();
}

@FindBy(how= How.ID, using = "Order_Account_Type")
	public WebElement Order_Account_Type;

public void verify_Order_Account_Type_Status(String data){
		//Verifies the Status of the Order_Account_Type
		Assert.assertEquals(Order_Account_Type,Order_Account_Type);
}

public void select_Order_Account_Type(){
		Order_Account_Type.click();
}

@FindBy(how= How.ID, using = "Order_CHECKINGS")
	public WebElement Order_CHECKINGS;

public void verify_Order_CHECKINGS_Status(String data){
		//Verifies the Status of the Order_CHECKINGS
		Assert.assertEquals(Order_CHECKINGS,Order_CHECKINGS);
}

public void select_Order_CHECKINGS(){
		Order_CHECKINGS.click();
}

@FindBy(how= How.ID, using = "Order_SAVINGS")
	public WebElement Order_SAVINGS;

public void verify_Order_SAVINGS_Status(String data){
		//Verifies the Status of the Order_SAVINGS
		Assert.assertEquals(Order_SAVINGS,Order_SAVINGS);
}

public void select_Order_SAVINGS(){
		Order_SAVINGS.click();
}

@FindBy(how= How.ID, using = "Order_Account")
	public WebElement Order_Account;

public void verify_Order_Account_Status(String data){
		//Verifies the Status of the Order_Account
		Assert.assertEquals(Order_Account,Order_Account);
}

public void select_Order_Account(){
		Order_Account.click();
}

@FindBy(how= How.ID, using = "Order_12345")
	public WebElement Order_12345;

public void verify_Order_12345_Status(String data){
		//Verifies the Status of the Order_12345
		Assert.assertEquals(Order_12345,Order_12345);
}

public void select_Order_12345(){
		Order_12345.click();
}

@FindBy(how= How.ID, using = "Order_98745")
	public WebElement Order_98745;

public void verify_Order_98745_Status(String data){
		//Verifies the Status of the Order_98745
		Assert.assertEquals(Order_98745,Order_98745);
}

public void select_Order_98745(){
		Order_98745.click();
}

@FindBy(how= How.ID, using = "Order_Transfer_Funds")
	public WebElement Order_Transfer_Funds;

public void verify_Order_Transfer_Funds_Status(String data){
		//Verifies the Status of the Order_Transfer_Funds
		Assert.assertEquals(Order_Transfer_Funds,Order_Transfer_Funds);
}

public void select_Order_Transfer_Funds(){
		Order_Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Order_unnamed")
	public WebElement Order_unnamed;

public void verify_Order_unnamed_Status(String data){
		//Verifies the Status of the Order_unnamed
		Assert.assertEquals(Order_unnamed,Order_unnamed);
}

public void select_Order_unnamed(){
		Order_unnamed.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}