com.conformiq.creator.structure.v15
creator.customaction qmldfc3c3a6642148898bb10e3c80b94747 "closebrowser"
	interfaces = [ qml2b95b855a5b54f9ca6f885c89b7b359f
	deleted ]
	shortform = "CB"
	direction = in
	tokens = [ literal "Close Browser" ]
	deleted
{
}
creator.externalinterface qml2b95b855a5b54f9ca6f885c89b7b359f "User"
	direction = in
	deleted;
creator.gui.screen qmla4eb52355ee44e8283a98d6c46ec78a8 "S1"
	deleted
{
	creator.gui.hyperlink qmlb08198ee38684bf881833a54e8949928 "Next"
		status = dontcare
		deleted;
}
creator.gui.screen qml016b3d8319f74ac7962de50a934ca50b "S2"
	deleted
{
}
creator.gui.screen qml8c78a1c2b59644a49088e30322293b9f "Screen1"
{
	creator.gui.hyperlink qml0e75f1d394d544cbbc57ebdcc04e4e7c "HyperLink"
		status = dontcare;
	creator.gui.form qmlfb9f332bcec04187b5d675c0fb1728de "Userdetails"
	{
		creator.gui.textbox qml64f81f30e17f4808bdeb61d3cbc26853 "FACalldate"
			type = number
			status = dontcare;
		creator.gui.textbox qmle819975299d4422794a120fcac445a11 "FSCallDate"
			type = String
			status = dontcare;
	}
}
creator.gui.screen qmlcc9ce2e0e0e94511a1d0b6930c03ab72 "Screen2"
{
}
creator.customaction qml7ea93f21e6264ba6a8826b112ec217db "CloseBrowser"
	interfaces = [ qml5a24f6c72d324f6fab69d705707891bc ]
	shortform = "CB"
	direction = in
	tokens = [ literal "Close Browser" ]
{
}
creator.externalinterface qml5a24f6c72d324f6fab69d705707891bc "User"
	direction = in;
