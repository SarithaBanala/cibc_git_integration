com.conformiq.creator.structure.v15
creator.customaction qml60ca8eae0d9b4c6ca8d65de59f4d8fcc "RightClick"
	interfaces = [ qml6e17e70fb2244523819a14ecae500a1c ]
	shortform = "RC"
	direction = in
	tokens = [ literal "Right Click" ]
{
	creator.primitivefield qml7e167dbb8c2241458318cf56a10eaee8 "unnamed"
		type = String
		deleted;
}
creator.externalinterface qml6e17e70fb2244523819a14ecae500a1c "User1"
	direction = in;
creator.customaction qmlc3a463c9f49c4ee8a52de9f1c79dcf40 "DisplayMessage"
	interfaces = [ qmld760b94e2c9d4321b16e6c84e89c6cc3 ]
	shortform = "VT"
	direction = out
	tokens = [ literal "Verify Status" reference
qml871b83b961064d84aeae86818a9eac7c ]
{
	creator.primitivefield qml871b83b961064d84aeae86818a9eac7c "Verify"
		type = String;
}
creator.externalinterface qmld760b94e2c9d4321b16e6c84e89c6cc3 "Display"
	direction = out;
creator.gui.popup qml891cd57aa1be47d6bdd540834a85413b "RightClickOptions"
{
	creator.gui.hyperlink qml12eb9c842d674cb88f3b5d1e6ff1fd56 "Open Link"
		status = dontcare;
}
